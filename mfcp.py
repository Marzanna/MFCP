import struct
import os
import pyotherside

def open_file(filename):
    a_data = [] #all data will be stored here
    a_sector = [] #all sectors
    a_block = ["", "", ""] #each block

    try:
        mfd = open(filename, "rb")
    except:
        pyotherside.send("error", 1) # Can't open file
        return
    mfd.seek(0, os.SEEK_END)
    if mfd.tell() < 1024:
        pyotherside.send("error", 2) # File is too small
        mfd.close()
        return
    mfd.seek(0)
    uid = mfd.read(4)
    uid = struct.unpack("BBBB", uid)
    p_uid = "" # Here full UID will be stored
    for i in range(4):
        p_uid += "{:02x}".format(uid[i])
    a_block[0] = p_uid

    bcc = mfd.read(1)
    bcc = struct.unpack("B", bcc)
    bcc = "{0:0x}".format(bcc[0]).zfill(2)
    a_block[1] = bcc

    rem = mfd.read(11)
    rem = struct.unpack("BBBBBBBBBBB", rem)
    p_rem = ""
    for i in range(11):
        p_rem += "{:02x}".format(rem[i])
    a_block[2] = p_rem

    a_sector.append(a_block.copy())

    #Read the rest of the first (zero) sector
    for n in range(3):
        key_a = mfd.read(6)
        ab = mfd.read(4)
        key_b = mfd.read(6)
        key_a = struct.unpack("BBBBBB", key_a)
        ab = struct.unpack("BBBB", ab)
        key_b = struct.unpack("BBBBBB", key_b)
        p_key_a, p_ab, p_key_b = "", "", ""
        for i in range(6):
            p_key_a += "{:02x}".format(key_a[i])
            p_key_b += "{:02x}".format(key_b[i])
        for i in range(4):
            p_ab += "{:02x}".format(ab[i])
        a_block[0] = p_key_a
        a_block[1] = p_ab
        a_block[2] = p_key_b
        a_sector.append(a_block.copy())
    a_data.append(a_sector.copy())

    for m in range(15):
        a_sector = []
        for n in range(4):
            key_a = mfd.read(6)
            ab = mfd.read(4)
            key_b = mfd.read(6)

            key_a = struct.unpack("BBBBBB", key_a)
            ab = struct.unpack("BBBB", ab)
            key_b = struct.unpack("BBBBBB", key_b)
            p_key_a, p_ab, p_key_b = "", "", ""
            for i in range(6):
                p_key_a += "{:02x}".format(key_a[i])
                p_key_b += "{:02x}".format(key_a[i])
            for i in range(4):
                p_ab += "{:02x}".format(ab[i])
            a_block[0] = p_key_a
            a_block[1] = p_ab
            a_block[2] = p_key_b
            a_sector.append(a_block.copy())
        a_data.append(a_sector.copy())

    pyotherside.send("aData", a_data)
    mfd.close()
