#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QIcon>
#include "QmlClipboardAdapter.h"

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QGuiApplication app(argc, argv);

    qmlRegisterType<QmlClipboardAdapter>("mfcp", 1, 0, "QClipboard");

    QQmlApplicationEngine engine;
    engine.load(QUrl(QLatin1String("qrc:/main.qml")));
    app.setWindowIcon(QIcon(":MFCP.png"));

    return app.exec();
}
