import QtQuick
import QtQuick.Controls
import QtQuick.Layouts
import QtQuick.Dialogs
import io.thp.pyotherside

import mfcp

ApplicationWindow {
    QClipboard {
        id: clipboard
    }

    id: window
    visible: true
    width: 300
    height: 1000
    property string clData: "";
    title: qsTr("Mifare Classic Parser");
    Dialog{
        id: errorDialog
        property string errText: ""
        visible: false
        title: qsTr("Error!");
        Label{
        id: errLabel
            text: errorDialog.errText
        }
    }

    FileDialog {
        id: fileDialog
        title: qsTr("Please choose a dump file");
        onAccepted: {
            var path = fileDialog.selectedFile.toString().replace(/^(file:\/{2})/,"");
            console.log("You chose: " + decodeURIComponent(path));
            python.call("mfcp.open_file", [decodeURIComponent(path)], function(){});
        }
        onRejected: {
            console.log("Canceled")
        }
        //Component.onCompleted: visible = true
    }

    ColumnLayout {
        anchors.fill: parent
        RowLayout{
            id: buttonsRow
            Button{
                text: qsTr("Open dump file");
                onClicked: fileDialog.visible = true
            }
            Button{
                text: qsTr("Copy raw data to clipboard");
                onClicked: clipboard.setText(window.clData);
                }
        }

        ListView{
            id: listView
            //anchors.top: buttonsRow.bottom
            //height: contentHeight
            //width: contentWidth
            clip: true
            flickableDirection: Flickable.VerticalFlick
            boundsBehavior: Flickable.StopAtBounds
            //interactive: true
            model: ListModel {
                id: listModel
            }
            delegate: MDelegate{}
            Layout.fillWidth: true
            Layout.fillHeight: true
            ScrollBar.vertical: ScrollBar {}
        }
    }

    Python{
        id: python
        Component.onCompleted: {
            addImportPath(Qt.resolvedUrl('.'));
            importModule('mfcp', function(){});
            setHandler('aData', function(data) {
                for(var i = 0; i < data.length; i++) {
                    listModel.append({"A0": data[i][0][0], "AB0": data[i][0][1], "B0": data[i][0][2],
                                    "A1": data[i][1][0], "AB1": data[i][1][1], "B1": data[i][1][2],
                                    "A2": data[i][2][0], "AB2": data[i][2][1], "B2": data[i][2][2],
                                    "A3": data[i][3][0], "AB3": data[i][3][1], "B3": data[i][3][2],
                                    "A0text": "Sector" + i + ", Block0, KeyA",
                                    "AB0text": "Sector" + i + ", Block0, Access Bits",
                                    "B0text": "Sector" + i + ", Block0, KeyB",
                                    "A1text": "Sector" + i + ", Block1, KeyA",
                                    "AB1text": "Sector" + i + ", Block1, Access Bits",
                                    "B1text": "Sector" + i + ", Block1, KeyB",
                                    "A2text": "Sector" + i + ", Block2, KeyA",
                                    "AB2text": "Sector" + i + ", Block2, Access Bits",
                                    "B2text": "Sector" + i + ", Block2, KeyB",
                                    "A3text": "Sector" + i + ", Block3, KeyA",
                                    "AB3text": "Sector" + i + ", Block3, Access Bits",
                                    "B3text": "Sector" + i + ", Block3, KeyB",
                                    "sector": "Sector" + i}
                    );
                }
                // Make data ready to be copied to clipboard
                for(var i = 0; i < data.length; i++) {
                    for(var j = 0; j < 4; j++){
                        for(var m = 0; m < 3; m++) {
                            window.clData += data[i][j][m];
                        }
                    }
                }
            });
            setHandler("error", function(err){
                if(err === 1) {
                    errorDialog.errText = qsTr("Can't open file!");
                } else if(err === 2) {
                    errorDialog.errText = qsTr("File is too small!");
                }
                errorDialog.visible = true;
            });
        }

        onError: {
            // when an exception is raised, this error handler will be called
            console.log('python error: ' + traceback);
        }
        onReceived: {
            // asychronous messages from Python arrive here
            // in Python, this can be accomplished via pyotherside.send()
            console.log('got message from python: ' + data);
        }
    }
}
