import QtQuick
import QtQuick.Controls

Label {
    anchors.horizontalCenter: parent.horizontalCenter
    anchors.verticalCenter: parent.verticalCenter
    font.family: "Monospace"
    ToolTip.visible: mouseArea.hovered
    MouseArea {
        property bool hovered
        id: mouseArea
        hoverEnabled: true
        anchors.fill: parent
        onEntered: hovered = true
        onExited: hovered = false
    }
}
