**MiFare Classic Parser**

This is a simple application to view Mifare classic dumps (.mfd files) in a convenient way.

You can also copy raw data to clipboard as a simple text.

You need Python 3, QtQML and PyOtherSide 1.5 to run this application.

![Screenshot](https://cloud.moemoekyun.moe/index.php/s/HHDw2eLdS59ek2g/preview)
