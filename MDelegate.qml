import QtQuick
import QtQuick.Controls

//A very bad code
Rectangle{
    property int spacing: 4
    property int rspacing: 2
    border.color: "black"
    border.width: 1
    color: "green"
    radius: 5
    width: col.width + 8
    height: col.height + secLabel.height + 8
    Label {
        id: secLabel
        text: sector
    }
    Column {
        id: col
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter
        Row {
            id: row0
            padding: rspacing
            spacing: rspacing
            ARectangle{
                width: a0Label.contentWidth + spacing
                height: a0Label.contentHeight + spacing
                MLabel{
                    id: a0Label
                    text: A0
                    ToolTip.text: (index == 0) ? "UID":A0text
                }
            }
            ABRectangle {
                width: ab0Label.contentWidth + spacing
                height: ab0Label.contentHeight + spacing
                MLabel {
                    id: ab0Label
                    text: AB0
                    ToolTip.text: (index == 0) ? "BCC":AB0text
                }
            }
            BRectangle{
                width: b0Label.contentWidth + spacing
                height: b0Label.contentHeight + spacing
                MLabel{
                    id: b0Label
                    text: B0
                    ToolTip.text: (index == 0) ? "REM":B0text
                }
            }
        }
        Row {
            id: row1
            padding: rspacing
            spacing: rspacing
            ARectangle {
                width: a1Label.contentWidth + spacing
                height: a1Label.contentHeight + spacing
                MLabel {
                    id: a1Label
                    text: A1
                    ToolTip.text: A1text
                }
            }
            ABRectangle {
                width: ab1Label.contentWidth + spacing
                height: ab1Label.contentHeight + spacing
                MLabel {
                    id: ab1Label
                    text: AB1
                    ToolTip.text: AB1text
                }
            }
            BRectangle {
                width: b1Label.contentWidth + spacing
                height: b1Label.contentHeight + spacing
                MLabel{
                    id: b1Label
                    text: B1
                    ToolTip.text: B1text
                }
            }
        }
        Row {
            id: row2
            padding: rspacing
            spacing: rspacing
            ARectangle {
                width: a2Label.contentWidth + spacing
                height: a2Label.contentHeight + spacing
                MLabel {
                    id: a2Label
                    text: A2
                    ToolTip.text: A2text
                }
            }
            ABRectangle {
                width: ab2Label.contentWidth + spacing
                height: ab2Label.contentHeight + spacing
                MLabel {
                    id: ab2Label
                    text: AB2
                    ToolTip.text: AB2text
                }
            }
            BRectangle {
                width: b2Label.contentWidth + spacing
                height: b2Label.contentHeight + spacing
                MLabel{
                    id: b2Label
                    text: B2
                    ToolTip.text: B2text
                }
            }
        }
        Row {
            id: row3
            padding: rspacing
            spacing: rspacing
            ARectangle {
                width: a3Label.contentWidth + spacing
                height: a3Label.contentHeight + spacing
                MLabel {
                    id: a3Label
                    text: A3
                    ToolTip.text: A3text
                }
            }
            ABRectangle {
                width: ab3Label.contentWidth + spacing
                height: ab3Label.contentHeight + spacing
                MLabel {
                    id: ab3Label
                    text: AB3
                    ToolTip.text: AB3text
                }
            }
            BRectangle {
                width: b3Label.contentWidth + spacing
                height: b3Label.contentHeight + spacing
                MLabel {
                    id: b3Label
                    text: B3
                    ToolTip.text: B3text
                }
            }
        }
    }
}
